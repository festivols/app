from flask import current_app, flash, redirect, render_template, url_for, request
from flask_wtf import FlaskForm
from flask_user.forms import RegisterForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError, SelectMultipleField, SelectField, widgets,DateField, SelectField, IntegerField
from wtforms.validators import DataRequired, Email, EqualTo
from flask_user import UserManager
from flask_login import current_user, login_user, logout_user
from flask_user import signals
from datetime import datetime


from .models import User, Role, Team


class CustomRegistrationForm(RegisterForm):
    """
    Form for users to create new account
    """
    last_name   = StringField('Nom', validators=[DataRequired()])
    first_name  = StringField('Prénom', validators=[DataRequired()])
    address     = StringField('Adresse', validators=[DataRequired()])
    phone       = StringField('Téléphone', validators=[DataRequired()])
    birthday    = StringField('Date d\'anniversaire', validators=[DataRequired()])


class CustomUserManager(UserManager):

    def customize(self, app):

        # Configure customized forms
        self.RegisterFormClass = CustomRegistrationForm
        # NB: assign:  xyz_form = XyzForm   -- the class!
        #   (and not:  xyz_form = XyzForm() -- the instance!)

    def register_view(self):
        #  https://github.com/lingthio/Flask-User/blob/master/flask_user/user_manager__views.py
        """ Display registration form and create new User."""
        safe_next_url = self._get_safe_next_url('next', self.USER_AFTER_LOGIN_ENDPOINT)
        safe_reg_next_url = self._get_safe_next_url('reg_next', self.USER_AFTER_REGISTER_ENDPOINT)

        # Initialize form
        login_form = self.LoginFormClass()  # for login_or_register.html
        register_form = self.RegisterFormClass(request.form)  # for register.html

        # invite token used to determine validity of registeree
        invite_token = request.values.get("token")

        # require invite without a token should disallow the user from registering
        if self.USER_REQUIRE_INVITATION and not invite_token:
            flash("Registration is invite only", "error")
            return redirect(url_for('user.login'))

        user_invitation = None
        if invite_token and self.db_manager.UserInvitationClass:
            data_items = self.token_manager.verify_token(invite_token, self.USER_INVITE_EXPIRATION)
            if data_items:
                user_invitation_id = data_items[0]
                user_invitation = self.db_manager.get_user_invitation_by_id(user_invitation_id)

            if not user_invitation:
                flash("Invalid invitation token", "error")
                return redirect(url_for('user.login'))

            register_form.invite_token.data = invite_token

        if request.method != 'POST':
            login_form.next.data = register_form.next.data = safe_next_url
            login_form.reg_next.data = register_form.reg_next.data = safe_reg_next_url
            if user_invitation:
                register_form.email.data = user_invitation.email

        # Process valid POST
        if request.method == 'POST' and register_form.validate():
            user = self.db_manager.add_user()
            register_form.populate_obj(user)
            user_email = self.db_manager.add_user_email(user=user, is_primary=True)
            register_form.populate_obj(user_email)

            # Store password hash instead of password
            user.password = self.hash_password(user.password)


            ## ADD A DEFAULT ROLE
            if not Role.query.filter_by(name='admin').first():
                role = Role.find_or_create_role(name='admin', label='organisateur')
                user.roles.append(role)
            else:
                role = Role.find_or_create_role(name='volunteer', label='bénévole')
                user.roles.append(role)

            # Email confirmation depends on the USER_ENABLE_CONFIRM_EMAIL setting
            request_email_confirmation = self.USER_ENABLE_CONFIRM_EMAIL
            # Users that register through an invitation, can skip this process
            # but only when they register with an email that matches their invitation.
            if user_invitation:
                if user_invitation.email.lower() == register_form.email.data.lower():
                    user_email.email_confirmed_at=datetime.utcnow()
                    request_email_confirmation = False

            self.db_manager.save_user_and_user_email(user, user_email)
            self.db_manager.commit()

            # Send 'registered' email and delete new User object if send fails
            if self.USER_SEND_REGISTERED_EMAIL:
                try:
                    # Send 'confirm email' or 'registered' email
                    self._send_registered_email(user, user_email, request_email_confirmation)
                except Exception as e:
                    # delete new User object if send  fails
                    self.db_manager.delete_object(user)
                    self.db_manager.commit()
                    raise

            # Send user_registered signal
            signals.user_registered.send(current_app._get_current_object(),
                                         user=user,
                                         user_invitation=user_invitation)

            # Redirect if USER_ENABLE_CONFIRM_EMAIL is set
            if self.USER_ENABLE_CONFIRM_EMAIL and request_email_confirmation:
                safe_reg_next_url = self.make_safe_url(register_form.reg_next.data)
                return redirect(safe_reg_next_url)

            # Auto-login after register or redirect to login page
            if 'reg_next' in request.args:
                safe_reg_next_url = self.make_safe_url(register_form.reg_next.data)
            else:
                safe_reg_next_url = self._endpoint_url(self.USER_AFTER_CONFIRM_ENDPOINT)
            if self.USER_AUTO_LOGIN_AFTER_REGISTER:
                return self._do_login_user(user, safe_reg_next_url)  # auto-login
            else:
                return redirect(url_for('user.login') + '?next=' + quote(safe_reg_next_url))  # redirect to login page

        # Render form
        self.prepare_domain_translations()
        return render_template(self.USER_REGISTER_TEMPLATE,
                      form=register_form,
                      login_form=login_form,
                      register_form=register_form)


class EditUser(FlaskForm):
    """
    Form for users to create new account
    """
    last_name   = StringField('Nom', validators=[DataRequired()])
    first_name  = StringField('Prénom', validators=[DataRequired()])
    address     = StringField('Adresse', validators=[DataRequired()])
    email       = StringField('Email', validators=[DataRequired(), Email()])
    phone       = StringField('Téléphone', validators=[DataRequired()])
    birthday    = StringField('Date d\'anniversaire', validators=[DataRequired()])
    submit      = SubmitField('Enregistrer')

#https://gist.github.com/doobeh/4668212
class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=True)
    option_widget = widgets.CheckboxInput()



class AdminEditUser(FlaskForm):
    """
    Form for admins to add a role to an user
    """
    last_name   = StringField('Nom', validators=[DataRequired()])
    first_name  = StringField('Prénom', validators=[DataRequired()])
    address     = StringField('Adresse', validators=[DataRequired()])
    email       = StringField('Email', validators=[DataRequired(), Email()])
    phone       = StringField('Téléphone', validators=[DataRequired()])
    birthday    = StringField('Date d\'anniversaire', validators=[DataRequired()])


    """ Checkboxes : https://gist.github.com/doobeh/4668212 """



    roles        = MultiCheckboxField('Attribuer un(des) rôle(s)',
        coerce=int,
        validators=[DataRequired()])

    submit      = SubmitField('Enregistrer')


class AdminTeam(FlaskForm):
    """
    Form for admins to add a role to an user
    """
    name   = StringField('Nom d\'équipe', validators=[DataRequired()])
    teamleader  = SelectField('Choisir un responsable d\'équipe', coerce=int, validators=[DataRequired()])
    nb_max  = StringField('Nombre de volontaires maximum',validators=[DataRequired()], default=10)
    submit = SubmitField('Enregistrer')


class AdminPost(FlaskForm):
    """
    Form for admins to add a role to an user
    """
    name   = StringField('Nom du poste', validators=[DataRequired()])
    description  = StringField('Description du poste', validators=[DataRequired()])
    occurence  = StringField('Horaire du poste', validators=[DataRequired()])
    submit = SubmitField('Enregistrer')

class AdminAddShift(FlaskForm):
    team_id = SelectField('Choisir une équipe', validators=[DataRequired()])
    post_id = SelectField('Choisir un post', validators=[DataRequired()])
    nb_volunteers = StringField('Nombre de volontaires', validators=[DataRequired()], default=10)

    submit = SubmitField('Enregistrer')

class AdminEditShift(FlaskForm):
    team_id = SelectField('Choisir une équipe', validators=[DataRequired()])
    post_id = SelectField('Choisir un post', validators=[DataRequired()])
    nb_volunteers = StringField('Nombre de volontaires', validators=[DataRequired()])
    submit = SubmitField('Enregistrer')

class AdminTeamLeader(FlaskForm):
    """
    Form for admins to add a leader to a team
    """
    user_id = SelectField('Choisir un responsable', coerce=int, validators=[DataRequired()])
    team_id = SelectField('Choisir une équipe', coerce=int, validators=[DataRequired()])
    submit = SubmitField('Enregistrer')
