from flask import abort, render_template, render_template_string
from flask_user import roles_required, UserManager, current_user, login_required
from ..models import Role
from app import db
from . import home


@home.route('/')
@home.route('/index')
def index():
    return render_template('pages/index.jinja2', title="Welcome")
