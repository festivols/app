import pymysql
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import text
from config import Config

app = Flask(__name__)
# the next line is necessary with cPanel deployment
application = app

# change NOTHING below

# put them all together as a string that shows SQLAlchemy where the database is
app.config.from_object(Config)
# this variable, db, will be used for all SQLAlchemy commands
db = SQLAlchemy(app)

# NOTHING BELOW THIS LINE NEEDS TO CHANGE
# this route will test the database connection and nothing more
@app.route('/admin/testdb')
def testdb():
    try:
        db.session.query('1').from_statement(text('SELECT 1')).all()
        return '<h1>It works.</h1>'
    except Exception as e:
        # see Terminal for description of the error
        print("\nThe error:\n" + str(e) + "\n")
        return '<h1>Something is broken.</h1>'

@app.route('/admin/get_users')
def get_users():
    try:
        db.session.query('1').from_statement(text('SELECT * FROM `users`')).all()
        return '<h1>It works.</h1>'
    except Exception as e:
        # see Terminal for description of the error
        print("\nThe error:\n" + str(e) + "\n")
        return '<h1>Something is broken.</h1>'


if __name__ == '__main__':
    app.run(debug=True)
