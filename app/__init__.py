"""Initialize Flask app."""
from flask import Flask
from config import Config
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from flaskext.mysql import MySQL
import pymysql
from flask_login import LoginManager
from flask_user import UserManager

db = SQLAlchemy()

login_manager = LoginManager()
login_manager.login_view = 'login'
login_manager.login_message_category = 'danger'

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    Bootstrap(app)
    db.init_app(app)
    login_manager.init_app(app)


    # SETUP FLASK USER MANAGER
    from .models import User
    from .forms import CustomUserManager
    user_manager = CustomUserManager(app, db, User)

    with app.app_context():

        from .home import home

        from .accounts import account
        from .admin import admin

        app.register_blueprint(home)

        app.register_blueprint(account)
        app.register_blueprint(admin)

        db.create_all()

    return app
