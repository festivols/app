from flask import flash, redirect, render_template, url_for, request,render_template_string
from flask_login import login_required, login_user, logout_user, current_user
from app import db, login_manager

from . import account

from ..forms import EditUser

@account.route('/user')
@login_required
def account_user():
    return render_template('pages/account.jinja2', user=current_user)

@account.route('/user/edit', methods=['GET', 'POST'])
@login_required
def edit_user():
    form = EditUser()
    if form.validate_on_submit():
        current_user.last_name = form.last_name.data
        current_user.first_name = form.first_name.data
        current_user.address = form.address.data
        current_user.email = form.email.data
        current_user.phone = form.phone.data
        current_user.birthday = form.birthday.data
        db.session.commit()
        flash('Your changes have been saved.','success')
        return redirect(url_for('account.account_user'))

    elif  request.method == 'GET':
        form.last_name.data = current_user.last_name
        form.first_name.data = current_user.first_name
        form.address.data = current_user.address
        form.email.data = current_user.email
        form.phone.data = current_user.phone
        form.birthday.data = current_user.birthday

    return render_template('edit/edit_user.jinja2' , form=form)
