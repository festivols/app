from app import db, login_manager
from datetime import datetime
from flask_user import current_user, login_required, roles_required, UserManager, UserMixin
from functools import wraps
from hashlib import md5

from werkzeug.security import generate_password_hash, check_password_hash

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)

    # User authentication information (required for Flask-User)
    email               = db.Column(db.Unicode(255), nullable=False, server_default=u'', unique=True)
    email_confirmed_at  = db.Column(db.DateTime())
    is_blacklisted      = db.Column(db.Boolean(), nullable=False, default=False)
    password            = db.Column(db.String(255), nullable=False, server_default='')
    active              = db.Column(db.Boolean(), nullable=False, server_default='0')

    # #ExtraFields
    first_name          = db.Column(db.Unicode(50), nullable=False, server_default='')
    last_name           = db.Column(db.Unicode(50), nullable=False, server_default='')
    address             = db.Column(db.String(200), nullable=False, server_default='')
    phone               = db.Column(db.String(200), nullable=False, server_default='')
    birthday            = db.Column(db.String(200), nullable=False, server_default='')

    # Relationships
    roles = db.relationship('Role', secondary='users_roles',
                            backref=db.backref('users', lazy='dynamic'))

    def __repr__(self):
        return '<User {0} {1}>'.format(self.last_name, self.first_name)

    # generate an avatar icon for each user
    # https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-vi-profile-page-and-avatars
    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

class Role(db.Model):
    __tablename__ = 'roles'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), nullable=False, server_default=u'', unique=True)  # for @roles_accepted()
    label = db.Column(db.String(50), nullable=False, server_default=u'', unique=True)

    #https://github.com/lingthio/Flask-User-starter-app/blob/master/app/commands/init_db.py
    def find_or_create_role(name, label):
        """ Find existing role or create new role """
        role = Role.query.filter(Role.name == name).first()
        if not role:
            role = Role(name=name, label=label)
            db.session.add(role)
        return role
    #End

    def get_role_by_id(role_id):
        role = Role.query.filter(Role.id == role_id).first()
        if not role:
            return false;
        return role

    def get_role_by_name(role_name):
        role = Role.query.filter(Role.name == role_name).first()
        if not role:
            return false;
        return role

    def is_blacklisted(user_id):
        role = Role.query.filter(find_or_create_role('blacklisted', 'Blacklisté')).filter(User.user_id==user_id).first()
        if not role:
            return false;
        return role

    def isset_admin():
        role = Role.query.filter(Role.name == name).first()
        if not role:
            return false
        return True

    def remove_team_role(team_id):
        team_role = Role.query.filter(Role.name == 'team-'+str(team_id)).first()
        db.session.delete(team_role)
        db.session.commit()
        return False


    def __repr__(self):
       return '<Role: {}>'.format(self.name)

class UsersRoles(db.Model):
    __tablename__ = 'users_roles'

    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))

class Team(db.Model):
    __tablename__ = 'teams'

    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String(60),unique=True)
    nb_max      = db.Column(db.String(60))


    # # Relationships
    # users = db.relationship('User', secondary='users_team',
    #                         backref=db.backref('team', lazy='dynamic'))

    def get_id_by_name(team_name):
        team = Team.query.filter(Team.name == team_name).first()
        if not team:
            return false;
        return team

    def __repr__(self):
        return '<Team {}>'.format(self.team)

class UsersTeam(db.Model):
    __tablename__ = 'users_team'

    id = db.Column(db.Integer(), primary_key=True)
    team_id = db.Column(db.Integer(), db.ForeignKey('teams.id', ondelete='CASCADE'))
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    is_teamleader = db.Column(db.Boolean())

    def find_or_create_teamleader(team_id, user_id):
        teamleader = UsersTeam.query.filter(UsersTeam.user_id==user_id).filter(UsersTeam.team_id==team_id).filter(UsersTeam.is_teamleader==1).first()
        if not teamleader:
            teamleader = UsersTeam(team_id=team_id,user_id=user_id, is_teamleader=1)
            db.session.add(teamleader)
        return teamleader

    def get_team_leader_id(team_id):
        teamleader = UsersTeam.query.filter(UsersTeam.team_id==team_id).filter(UsersTeam.is_teamleader==1).first()
        try:
            teamleader_id = teamleader.user_id
        except:
            return None
        return teamleader_id


    def delete_teamleader(team_id):
        teamleader = UsersTeam.query.filter(UsersTeam.team_id==team_id).filter(UsersTeam.is_teamleader==1).first()
        db.session.delete(teamleader)

class Post(db.Model):
    __tablename__ = 'posts'
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String(60),unique=True)
    description = db.Column(db.String(255), unique=False)
    occurence   = db.Column(db.String(255))
    #team_id     = db.Column(db.Integer, db.ForeignKey('team.id'), nullable=False)

class Shift(db.Model):
    __tablename__ = 'shifts'
    id          = db.Column(db.Integer, primary_key=True)
    post_id     = db.Column(db.Integer)
    team_id     = db.Column(db.Integer)
    nb_volunteers = db.Column(db.Integer,default=10)

    def find_or_create_shift(team_id, post_id, nb_volunteers):
        shift = Shift.query.filter(Shift.post_id==post_id).filter(Shift.team_id==team_id).filter(Shift.nb_volunteers==nb_volunteers).first()
        if not shift:
            shift = Shift(team_id=team_id,post_id=post_id, nb_volunteers=nb_volunteers)
            db.session.add(shift)
        return shift
