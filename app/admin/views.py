from flask import abort, render_template_string, render_template, request, flash, redirect, url_for
from flask_user import current_user, login_required, roles_required
from app import db
from . import admin
from ..models import User, Role, Team, Post, UsersTeam, UsersRoles, Shift
from ..forms import AdminEditUser, AdminTeam, AdminPost, AdminTeamLeader, AdminAddShift, AdminEditShift


######################### ADMIN INDEX #########################

@admin.route('/admin')
@admin.route('/admin/')
@roles_required('admin')
def admin_page():
    return render_template('admin/admin.jinja2')

######################### USERS #########################

@admin.route('/admin/users/all')
@roles_required('admin')
def all_users():
    users = User.query.filter(User.is_blacklisted==False).all()
    return render_template('admin/user/all_users.jinja2' , users=users, blacklist=False)

# Edit users and its role
@admin.route('/admin/users/id/<int:user_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def admin_edit_user(user_id):
    form = AdminEditUser()
    user = User.query.get(user_id)
    role = Role.query.all()

    if  request.method == 'GET':
        form.roles.choices = [(role.id, role.label) for role in role ]
        form.roles.default = [(role.id) for role in user.roles ]
        form.process()

        form.last_name.data = user.last_name
        form.first_name.data = user.first_name
        form.address.data = user.address
        form.email.data = user.email
        form.phone.data = user.phone
        form.birthday.data = user.birthday

    else:
        user.last_name = form.last_name.data
        user.first_name = form.first_name.data
        user.address = form.address.data
        user.email = form.email.data
        user.phone = form.phone.data
        user.birthday = form.birthday.data
        for user_role in user.roles:
            try:
                user.roles[user_role.id] = form.roles.data[user_role.id]
            except:
                role_tmp = Role.query.get(user_role.id)
                user.roles.remove(role_tmp)

        for role_id in form.roles.data:
            try:
                role = Role.get_role_by_id(role_id)
                if role not in user.roles:
                    user.roles.append(role)
            except:
                flash('Error, the role do not exist')

        db.session.commit()
        flash('Your changes have been saved.','success')
        return redirect(url_for('admin.all_users'))

    return render_template('admin/user/edit_one_user.jinja2', form=form, user_id=user_id, user=user)

# Delete user
@admin.route('/admin/users/delete/id/<int:user_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def admin_delete_user(user_id):
    user = User.query.get(user_id)
    db.session.delete(user)
    db.session.commit()
    flash('Your changes have been saved.','success')
    return redirect(url_for('admin.all_users'))

######################### TEAMS #########################

@admin.route('/admin/teams/all')
@roles_required('admin')
def all_teams():
    teams = Team.query.all()
    for team in teams:
        try:
            query = UsersTeam.query.filter(UsersTeam.is_teamleader == 1).filter(UsersTeam.team_id == team.id)
            user_id = query[0].user_id
            user = User.query.filter(User.id==user_id).first()
            team.last_name = user.last_name
            team.first_name = user.first_name
            teamrole=Role.query.filter(Role.name=='team-'+str(team.id)).first()
            nb_vol=UsersRoles.query.filter(UsersRoles.role_id==teamrole.id).count()
            team.nb_vol=nb_vol
        except Exception as e:

            team.last_name = "N/D"


    return render_template('admin/team/all_teams.jinja2' , teams=teams)

# Create Team
@admin.route('/admin/teams/create', methods = ['GET', 'POST'])
@roles_required('admin')
def create_team():
    form = AdminTeam()
    users=User.query.filter(User.is_blacklisted == 0).all()

    if  request.method == 'GET':
        form.teamleader.choices = [(0,'-')]
        form.teamleader.choices += [(user.id, user.last_name + ' ' +  user.first_name) for user in users]
        form.process()

    else:
        team = Team(
            name=form.name.data,
            nb_max=form.nb_max.data
        )
        db.session.add(team)
        db.session.commit()
        flash('L’équipe « '+form.name.data+' » a bien été créée', 'success')
        if form.teamleader.data != 0:
            team=Team.get_id_by_name(form.name.data)
            user_id=form.teamleader.data
            UsersTeam.find_or_create_teamleader(
                team_id=team.id,
                user_id=user_id
            )
            user=User.query.get(user_id)
            team_role = Role.find_or_create_role('team-'+str(team.id), 'Équipe « '+team.name+' »')
            user.roles.append(team_role)
            db.session.commit()
            flash('Le chef de l\'équipe « '+form.name.data+' » est désormais '+user.first_name+' '+user.last_name, 'success')
        return redirect(url_for('admin.all_teams'))
    return render_template("admin/team/create_team.jinja2", form=form)

# Edit team
@admin.route('/admin/teams/id/<int:team_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def edit_team(team_id):
    form = AdminTeam()
    team = Team.query.get(team_id)
    users = User.query.filter(User.is_blacklisted == 0).all()


    if  request.method == 'GET':
        form.teamleader.choices = [(0,'-')]
        form.teamleader.choices += [(user.id, user.last_name + ' ' +  user.first_name) for user in users]
        if not UsersTeam.get_team_leader_id(team_id):
            form.teamleader.default = 0
        else:
            form.teamleader.default = UsersTeam.get_team_leader_id(team_id)
        form.process()
        form.name.data = team.name

    else:
        team.name = form.name.data
        try:
            user_id=UsersTeam.get_team_leader_id(team.id)
            team_role = Role.query.filter(Role.name=='team-'+str(team.id)).first()
            previous_teamleader=User.query.get(user_id)
            previous_teamleader.roles.remove(team_role)
            UsersTeam.delete_teamleader(team.id)
            db.session.commit()
        except:
            new_team=1
        user_id=form.teamleader.data
        UsersTeam.find_or_create_teamleader(
            team_id=team_id,
            user_id=user_id
        )
        team_role = Role.find_or_create_role('team-'+str(team.id), 'Équipe « '+team.name+' »')
        new_teamleader=User.query.get(user_id)
        new_teamleader.roles.append(team_role)
        db.session.commit()
        flash('Les changements ont été sauvegardé','success')
        return redirect(url_for('admin.all_teams'))


    return render_template('admin/team/edit_one_team.jinja2', form=form, team=team, )


# Delete team
@admin.route('/admin/teams/delete/id/<int:team_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def delete_team(team_id):
    team=Team.query.get(team_id)
    Role.remove_team_role(team_id)
    db.session.delete(team)
    db.session.commit()
    flash('L\'équipe a bien été supprimée.','warning')
    return redirect(url_for('admin.all_teams'))

######################### POSTES #########################

@admin.route('/admin/posts/all')
@roles_required('admin')
def all_posts():
    posts = Post.query.all()
    return render_template('admin/post/all_posts.jinja2' , posts=posts)

# Create Posts
@admin.route('/admin/posts/create', methods = ['GET', 'POST'])
@roles_required('admin')
def create_post():
    form = AdminPost()
    if form.validate_on_submit():
        post = Post(
            name=form.name.data,
            description=form.description.data,
            occurence=form.occurence.data
        )
        db.session.add(post)
        db.session.commit()
        flash('Un poste a été créé', 'success')
        return redirect(url_for('admin.all_posts'))
    return render_template("admin/post/create_post.jinja2", form=form)

# Edit Posts
@admin.route('/admin/posts/id/<int:post_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def edit_post(post_id):
    form = AdminPost()
    post = Post.query.get(post_id)
    if form.validate_on_submit():
        post.name = form.name.data
        post.description = form.description.data
        post.occurence = form.occurence.data
        db.session.commit()
        flash('Les changements ont été sauvegardé','success')
        return redirect(url_for('admin.all_posts'))

    elif  request.method == 'GET':
        form.name.data = post.name
        form.description.data = post.description
        form.occurence.data = post.occurence
        #form.posts.data = team.posts

    return render_template("admin/post/edit_one_post.jinja2", form=form, post_id=post_id, post=post)

# Delete post
@admin.route('/admin/posts/delete/id/<int:post_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def delete_post(post_id):
    post = Post.query.get(post_id)
    db.session.delete(post)
    db.session.commit()
    flash('Le poste a bien été supprimé','warning')
    return redirect(url_for('admin.all_posts'))


######################### Shifts #########################

@admin.route('/admin/shifts/all')
@roles_required('admin')
def all_shifts():
    shifts = Shift.query.all()

    for shift in shifts:
        try:
            shift.team_name = Team.query.get(shift.team_id).name
            shift.post_name = Post.query.get(shift.post_id).name
            shift.post_occurence = Post.query.get(shift.post_id).occurence
        except:
            new=1

    return render_template('admin/shifts/all_shifts.jinja2' , shifts=shifts)

# Create Shift
@admin.route('/admin/shifts/add', methods = ['GET', 'POST'])
@roles_required('admin')
def add_shift():
    posts = Post.query.all()
    teams = Team.query.all()
    form = AdminAddShift()

    if request.method == 'GET':
        form.team_id.choices = [(team.id, team.name) for team in teams]
        form.post_id.choices = [(post.id, post.name) for post in posts]
    else:
        shift = Shift(
            team_id=form.team_id.data,
            post_id=form.post_id.data,
            nb_volunteers=form.nb_volunteers.data
        )
        db.session.add(shift)
        db.session.commit()
        flash('L\'horaire a bien été ajouté','success')
        return redirect(url_for('admin.all_shifts'))

    return render_template("admin/shifts/create_shift.jinja2", form=form)

@admin.route('/admin/shifts/id/<int:shift_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def edit_shift(shift_id):
    form = AdminEditShift()
    shift = Shift.query.get(shift_id)
    posts = Post.query.all()
    teams = Team.query.all()

    if request.method == 'GET':
        form.team_id.choices = [(team.id, team.name) for team in teams]
        form.team_id.default = shift.team_id
        form.post_id.choices = [(post.id, post.name) for post in posts]
        form.post_id.default = shift.post_id
        form.nb_volunteers.data = Shift.query.get(shift_id).nb_volunteers
        form.process()
    else:
        db.session.delete(shift)
        Shift.find_or_create_shift(
            team_id=form.team_id.data,
            post_id=form.post_id.data,
            nb_volunteers=form.nb_volunteers.data
        )
        db.session.commit()
        flash('L\'horaire a bien été modifié','success')
        return redirect(url_for('admin.all_shifts'))



    return render_template("admin/shifts/edit_one_shift.jinja2", form=form, shift_id=shift_id, shift=shift)



@admin.route('/admin/shifts/delete/id/<int:shift_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def delete_shift(shift_id):
    shift = Shift.query.get(shift_id)

    db.session.delete(shift)
    db.session.commit()
    flash('Le shift a bien été supprimé','warning')
    return redirect(url_for('admin.all_shifts'))


######################### TEAM LEADER #########################

# Teamleader
@admin.route('/admin/teamleaders/all')
@roles_required('admin')
def all_teamleaders():
    teamleaders = UsersTeam.query.filter(UsersTeam.is_teamleader == 1).all()

    for userteam in teamleaders:
        userteam.lastname = User.query.get(userteam.user_id).last_name
        userteam.firstname = User.query.get(userteam.user_id).first_name
        userteam.phone = User.query.get(userteam.user_id).phone
        userteam.team = Team.query.get(userteam.team_id).name
        userteam.team_id = Team.query.get(userteam.team_id).id

    return render_template('admin/teamleader/all_teamleaders.jinja2', teamleaders=teamleaders)

# Add a leader to a team
@admin.route('/admin/teamleaders/add', methods = ['GET', 'POST'])
@roles_required('admin')
def add_teamleader():
    form = AdminTeamLeader()
    users = User.query.all()
    teams = Team.query.all()

    if request.method == 'GET':
        form.user_id.choices = [(user.id, user.last_name + ' ' +  user.first_name) for user in users]
        form.team_id.choices = [(team.id, team.name) for team in teams]
        form.process()
    else:
        UsersTeam.find_or_create_teamleader(
            team_id=form.team_id.data,
            user_id=form.user_id.data
        )
        db.session.commit()
        flash('Le chef d\'équipe a bien été ajouté','success')
        return redirect(url_for('admin.all_teamleaders'))
    return render_template('admin/teamleader/create_teamleader.jinja2', form=form)

# Edit teamleader of a team
@admin.route('/admin/teamleaders/id/<int:teamleader_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def edit_teamleader(teamleader_id):
    form = AdminTeamLeader()
    teamleader = UsersTeam.query.get(teamleader_id)
    users = User.query.all()
    teams = Team.query.all()

    if request.method == 'GET':
        form.user_id.choices = [(user.id, user.last_name + ' ' +  user.first_name) for user in users]
        form.team_id.choices = [(team.id, team.name) for team in teams]
        form.team_id.default = teamleader.team_id
        form.user_id.default = teamleader.user_id
        form.process()
    else:
        teamleader = UsersTeam.query.get(teamleader_id)
        db.session.delete(teamleader)
        db.session.commit()
        UsersTeam.find_or_create_teamleader(
            team_id=form.team_id.data,
            user_id=form.user_id.data
        )
        db.session.commit()
        flash('Le chef d\'équipe a bien été modifié','success')
        return redirect(url_for('admin.all_teamleaders'))

    return render_template('admin/teamleader/create_teamleader.jinja2', form=form, teamleader_id=teamleader_id, teamleader=teamleader)

# Delete TeamLeader
@admin.route('/admin/teamleaders/delete/id/<int:teamleader_id>', methods = ['GET', 'POST'])
@roles_required('admin')
def delete_teamleader(teamleader_id):
    teamleader = UsersTeam.query.get(teamleader_id)
    db.session.delete(teamleader)
    db.session.commit()
    flash('Le chef d\'équipe a bien été supprimé','warning')
    return redirect(url_for('admin.all_teamleaders'))

@admin.route('/admin/users/all/blacklisted')
@roles_required('admin')
def all_blacklisted():
    users = User.query.filter(User.is_blacklisted==1).all()
    return render_template('admin/user/all_users.jinja2' , users=users, blacklist=True)

# Delete TeamLeader
@admin.route('/admin/users/blacklist/id/<int:user_id>/<int:blacklist>', methods = ['GET', 'POST'])
@roles_required('admin')
def admin_blacklist_user(user_id, blacklist):
    user = User.query.get(user_id)
    user.roles.append(Role.find_or_create_role('blacklisted', 'Blacklisté'))
    user.roles.remove(Role.find_or_create_role('volunteer', 'Bénévole'))
    user.is_blacklisted=1

    flash('L\'utilisateur "'+user.last_name+' '+user.first_name+'" :  a bien été blacklisté','warning')
    db.session.commit()
    if blacklist==1:
        return redirect(url_for('admin.all_blacklisted'))
    else:
        return redirect(url_for('admin.all_users'))

@admin.route('/admin/users/whitelist/id/<int:user_id>/<int:blacklist>', methods = ['GET', 'POST'])
@roles_required('admin')
def admin_whitelist_user(user_id, blacklist):
    user = User.query.get(user_id)
    user.is_blacklisted=0
    user.roles.remove(Role.find_or_create_role('blacklisted', 'Blacklisté'))
    user.roles.append(Role.find_or_create_role('volunteer', 'Bénévole'))
    db.session.commit()
    flash('L\'utilisateur "'+user.last_name+' '+user.first_name+'" :  a bien été whitelisté','success')
    if blacklist==1:
        return redirect(url_for('admin.all_blacklisted'))
    else:
        return redirect(url_for('admin.all_users'))
