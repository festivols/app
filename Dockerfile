FROM python

# Setup the directory where dependencies will be, copy the dependencies file then installed it.
COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

# Make sure all packages are up-to-date
RUN apt-get update &&\
    apt-get upgrade -y

# Setup the app directory and copy the src files into it.
COPY wsgi.py /FestiVOLS/wsgi.py
COPY config_example.py /FestiVOLS/config.py

WORKDIR /FestiVOLS
COPY app/ /FestiVOLS/app

# Create the "app" user and its group, make him owner of the src files, then make
# him run the application for security purposes.
RUN groupadd -r app &&\
    useradd -r -g app -d /FestiVOLS/app -s /sbin/nologin -c "Docker image user" app &&\
    chown -R app:app /FestiVOLS/app
USER app

# Arguments for the ENTRYPOINT command. Can be override when image is run.
CMD ["--bind", "0.0.0.0:5000", "wsgi:app"]
