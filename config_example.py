# this file contains most of the configuration variables that your app needs.
# http://exploreflask.com/en/latest/organizing.html#definitions

from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

class Config:
 
    #General 
    SECRET_KEY = environ.get("SECRET_KEY")
    
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # Flask-User settings
    USER_APP_NAME = environ.get("USER_APP_NAME")      # Shown in and email templates and page footers
    USER_EMAIL_SENDER_NAME = environ.get("USER_EMAIL_SENDER_NAME")
    USER_EMAIL_SENDER_EMAIL = environ.get("USER_EMAIL_SENDER_EMAIL")
    USER_ENABLE_EMAIL = True      
    USER_ENABLE_USERNAME = False    
    USER_REQUIRE_RETYPE_PASSWORD = True    


    # Flask-Mail SMTP server settings
    MAIL_SERVER = environ.get("MAIL_SERVER")
    MAIL_PORT = environ.get("MAIL_PORT")
    MAIL_USE_SSL = False
    MAIL_USE_TLS = True
    MAIL_USERNAME = environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = environ.get("MAIL_PASSWORD")
    MAIL_DEFAULT_SENDER = environ.get("MAIL_DEFAULT_SENDER")


